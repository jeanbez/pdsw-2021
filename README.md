# I/O Bottleneck Detection and Tuning:<br/>Connecting the Dots using Interactive Log Analysis

Jean Luca Bez (1), Houjun Tang (1), Bing Xie (2), David Williams-Young (1), Rob Latham (3), Rob Ross (3), Sarp Oral (2), Suren Byna (1)

(1) Lawrence Berkeley National Laboratory
(2) Oak Ridge National Laboratory
(3) Argonne National Laboratory

This repository contains the interactive visualizations from our PDSW 2021 paper:

https://gitlab.com/jeanbez/pdsw-2021

## DXT Explorer

DTX Explorer is a tool to generate interactive data visualizations of Darshan I/O traces collected from HPC applications. Its goal is to provide an easy and interactive way for researchers and developers to explore their application's I/O behavior and detect possible I/O bottlenecks that are impacting performance. You can check it out at:

https://gitlab.com/jeanbez/dxt-explorer

```
git clone https://gitlab.com/jeanbez/dxt-explorer
```

You need to have Python 3 and install some required libraries:

```
pip install -r requirements.txt
```

You also need to have Darshan Utils installed (darshan-dxt-parser) and available in your path. Once you have the dependencies installed, you can run:

```
python3 explore.py DARSHAN_FILE_COLLECTED_WITH_DXT_ENABLE.darshan
```

DXT Explorer will generate a explore.html file with an interactive plot that you can open in any browser to explore. A sample .darshan file is provided inside samples.

## Contents

In the `additional-plots.tar.gz` file you can find all the interactive plots versions from our paper. These HTML files can be opened with your preferred browser to explore the operation, transfer size, and spatiality of DXT traces.
