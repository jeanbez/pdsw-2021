<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>I/O Bottleneck Detection and Tuning: Connecting the Dots using Interactive Log Analysis</title>

		<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i|IBM+Plex+Sans+Condensed:400,400i|IBM+Plex+Sans:100,100i,400,400i,700,700i|IBM+Plex+Serif:400,400i" rel="stylesheet">
        <link rel="stylesheet" href="css/default.css"/>

        <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-43946099-4"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-43946099-4');
		</script>
    </head>
    <body>

    	<p>
			<b class="label-black">COMPANION WEBSITE</b>
		</p>

		<h1>I/O Bottleneck Detection and Tuning:<br/>Connecting the Dots using Interactive Log Analysis</h1>

		<p>
			<em>Jean Luca Bez<sup>1</sup>, Houjun Tang<sup>1</sup>, Bing Xie<sup>2</sup>, David Williams-Young<sup>1</sup>, Rob Latham<sup>3</sup>, Rob Ross<sup>3</sup>, Sarp Oral<sup>2</sup>, Suren Byna<sup>1</sup></em>
		</p>

		<p class="institutions">
			<sup>1</sup> Lawrence Berkeley National Laboratory, <sup>2</sup> Oak Ridge National Laboratory, <sup>3</sup> Argonne National Laboratory
		</p>

		<h2>Contents</h2>

		<p class="information">
			This repository contains the interactive visualizations from our PDSW 2021 paper:
		</p>

		<p class="url">
			<a href="https://gitlab.com/jeanbez/pdsw-2021">https://gitlab.com/jeanbez/pdsw-2021</a>
		</p>

		<p>
			<img src="images/dxt-explorer.png" />
		</p>

		<p class="information">
			DTX Explorer is a tool to generate interactive data visualizations of Darshan I/O traces collected from HPC applications. Its goal is to provide an easy and interactive way for researchers and developers to explore their application's I/O behavior and detect possible I/O bottlenecks that are impacting performance. You can check it out at:
		</p>

		<p class="url">
			<a href="https://github.com/hpc-io/dxt-explorer">https://github.com/hpc-io/dxt-explorer</a>
		</p>

		<p class="code">
			git clone https://github.com/hpc-io/dxt-explorer
		</p>

		<p class="information">
			You need to have Python 3 and install some required libraries:
		</p>

		<p class="code">
			pip install -r requirements.txt
		</p>

		<p class="information">
			You also need to have Darshan Utils installed (<span>darshan-dxt-parser</span>) and available in your path. Once you have the dependencies installed, you can run:
		</p>

		<p class="code">
			python3 explore.py DARSHAN_FILE_COLLECTED_WITH_DXT_ENABLE.darshan
		</p>

		<p class="information">
			DXT Explorer will generate a explore.html file with an interactive plot that you can open in any browser to explore. A sample .darshan file is provided inside <span>samples</span>.
		</p>

		<h2>Interactive Plots</h2>

		<p class="information">
 			You can explore the following interactive plots:
		</p>

		<ul>
			<li>
				<a href="interactive/summit/flash-original/explore.html"><b class="label-yellow">FIGURE 3</b></a> FLASH-IO in Summit: best baseline execution (operation)
			</li>
			<li>
				<a href="interactive/summit/flash-optimized/explore.html"><b class="label-yellow">FIGURE 4</b></a> FLASH-IO in Summit: best optimized execution (operation)
			</li>
			<li>
				<a href="interactive/summit/openpmd-original/explore.html"><b class="label-blue">FIGURE 5</b></a> OpenPMD in Summit: best baseline write execution (operation)
			</li>
			<li>
				<a href="interactive/summit/openpmd-optimized/explore.html"><b class="label-blue">FIGURE 6</b></a> OpenPMD in Summit: best optimized write execution (operation)
			</li>
			<li>
				<a href="interactive/cori/openpmd-original/explore-transfer.html"><b class="label-blue">FIGURE 10</b></a> OpenPMD in Cori: best baseline write execution (transfer size)
			</li>
			<li>
				<a href="interactive/cori/openpmd-original/explore.html"><b class="label-gray">BONUS</b></a> OpenPMD in Cori: best baseline write execution (operation)
			</li>
			<li>
				<a href="interactive/cori/openpmd-optimized/explore-transfer.html"><b class="label-blue">FIGURE 11</b></a> OpenPMD in Cori: best optimized write execution (transfer size)
			</li>
			<li>
				<a href="interactive/cori/openpmd-optimized/explore.html"><b class="label-gray">BONUS</b></a> OpenPMD in Cori: best optimized write execution (operation)
			</li>
			<li>
				<a href="interactive/summit/e2e-original/explore.html"><b class="label-red">FIGURE 12</b></a> E2E Summit: best baseline execution (operation)
			</li>
			<li>
				<a href="interactive/summit/e2e-optimized/explore.html"><b class="label-red">FIGURE 13</b></a> E2E Summit: best optimized execution (operation)
			</li>
		</ul>

		<p class="information">
			Complementary plots that depic tge transfer size and spatiality can be found in the repository under <span class="inline-code">additional-plots.tar.gz</span>. You can clone the repository and open the <span class="inline-code">.html</span> file in your preferred browser.
		</p>

		<h2>Additional Experiments</h2>

		<p class="information">
			In this section, we present additional snapshots from the DXT Explorer tool used in the paper.
		</p>

		<h3>OpenPMD in Cori</h3>

		<p class="information">
			Our tests on Cori used 64 Haswell compute nodes, 16 ranks per node, and a total of 1024 processes. The total file size is &asymp;320 GB, with no compression set at the HDF5 level. We configured the kernel to write a few meshes and particles in 3D. The meshes are viewed as a grid of dimensions [64 &times; 32 &times; 32] of mini blocks whose dimensions are [64 &times; 32 &times; 32]. Thus, the actual mesh size is [65536 &times; 256 &times; 256]. The kernel runs for 10 iteration steps.
		</p>

		<p class="information">
			In Figure 1, we illustrate the I/O behavior of our baseline on Cori, which uses cray-mpich/7.7.10 MPI and the HDF5 develop version. The I/O kernel already uses collective buffering for writing data. However, our interactive analysis revealed that there are a large number of small writes to the same offset by all MPI ranks, pointing to non-collective HDF5 metadata operations. The mean runtime of the benchmark with five runs was 54.82s. Explicitly enabling the MPI-IO two-phase I/O using ROMIO hints and setting the collective buffer size to $32$ MB, the same of the stripe size, the mean runtime increased to 62.18s. On the other hand, tacking the issue presented in Figure 1, by enabling collective metadata operations in HDF5, the runtime drops to 32.62s, i.e., a 70% improvement. Combining deferred metadata writes and paged allocation further reduces the runtime to 30.84s, i.e., a 77% performance improvement as depicted in Figure 2.
		</p>

		<img src="images/060-paper-5-cori-8a-baseline-large.png" />
		<p class="information">Figure 1: Best execution of the baseline OpenPMD write. It is possible to see small non-collective operations related to HDF5 metadata calls.</p>

		<img src="images/067-paper-5-romio-collective-metadata-cache-paged.png" />
		<p class="information">Figure 2: Best execution of OpenPMD after tuned for Cori.</p>

		<h3>E2E Benchmark in Summit</h3>

		<p class="information">
			Figure 3 shows the behavior of E2E at POSIX and MPI-IO level in Summit. In the plot, we can see the same behavior as we saw in Cori, where a lot of the time is taken by rank 0 sequentially writing fill values to all of the defined variables (10 in this workload). Figure 4 shows the runtime for this optimized version, after explicitly disabling the data filling behavior.
		</p>

		<img src="images/005-paper-1-summit-baseline.png" />
		<p class="information">Figure 3: Best exeuction of the baseline E2E benchmark in Summit. It is possible to see the long time spent by rank $0$ sequentially writing fill values to file.</p>

		<img src="images/007-paper-1-summit-no-fill.png" />
		<p class="information">Figure 4: Best exeuction of E2E after tuned for Summit.</p>

		<div class="copyright">
			<p>
				<strong>LICENSE</strong>
			</p>
			<p>
				DXT Explorer Copyright (c) 2021, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy). All rights reserved.
			</p>
			<p>
				If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.
			</p>
			<p>
				<strong>NOTICE</strong>. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights.  As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit others to do so.
			</p>
		</p>

    </body>
</html>

